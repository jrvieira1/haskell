module Main where

import Data.Numbers.Primes
import Data.Bits
import Codec.Picture

main :: IO ()
main = savePngImage "out.png" $ ImageY8 $ generateImage f 7000 19

f :: Int -> Int -> Pixel8
f x y
   | testBit (primes !! x :: Word) y = minBound
-- | testBit x y = if elem' x primes then minBound else div maxBound 2
   | otherwise = maxBound

-- elem' x ~(p:ps)
--    | x > p = elem' x ps
--    | p == x = True
--    | otherwise = False
