{-# LANGUAGE PatternSynonyms #-}
-- https://gitlab.haskell.org/ghc/ghc/-/issues/25456

data R = R { x :: () } deriving Show

pattern P :: R
pattern P = R { x = () }

main :: IO ()
main = do

-- print  P  { x = () }  -- error
   print (P) { x = () }  -- works

   print  R  { x = () }  -- works
-- print (R) { x = () }  -- error

