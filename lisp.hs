{-# LANGUAGE MonoLocalBinds #-}

main :: IO ()
main = do
   print "ok"
   print $ map (show :: Double -> String) [234,42,0]
   print $ (read :: String -> Double) "42"

data Atom a where
   Nil :: Atom ()
   Pair :: Atom a -> Atom b -> Atom (a,b)
   Symbol :: Atom String
   Number :: Atom Word

car :: Atom (a,b) -> Atom a
car (Pair x _) = x

cdr :: Atom (a,b) -> Atom b
cdr (Pair _ x) = x

isNil :: Atom a -> Bool
isNil Nil = True
isNil _ = False

isPair :: Atom a -> Bool
isPair (Pair _ (Pair _ _)) = False
isPair (Pair _ _) = True
isPair _ = False

isList :: Atom a -> Bool
isList (Pair _ (Pair _ _)) = True
isList _ = False

isSymbol :: Atom a -> Bool
isSymbol Symbol = True
isSymbol _ = False

isNumber :: Atom a -> Bool
isNumber Number = True
isNumber _ = False

