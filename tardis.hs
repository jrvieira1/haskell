-- https://blog.csongor.co.uk/time-travel-in-haskell-for-dummies/
-- https://danilafe.com/blog/haskell_lazy_evaluation/

import Data.Traversable ( mapAccumR, mapAccumL )

main :: IO ()
main = do
   print $ foldrmax ([3,0,7,8,2,9,3] :: [] Word)
   print $ genmax ([3,0,7,8,2,9,3] :: [] Word)
   print $ genavg ([3,0,7,8,2,9,3] :: [] Word)


{- EVERYTHING IS A FOLD
-- replace all elements of a list with the maximum element
-- single pass
-}

foldrmax :: (Ord a, Bounded a) => [a] -> [a]
foldrmax xs = xs'
   where
   (xs',xmax) = foldr (\x (acc,xm) -> (xmax : acc , max x xm)) (mempty,minBound) xs

{- xmax is a future value -}

-- generalize to traversable
genmax :: (Traversable t, Bounded a, Ord a) => t a -> t a
genmax t = xs'
   where
   (xmax,xs') = mapAccumR (\a b -> (max a b , xmax)) minBound t

-- more fun
genavg :: (Traversable t, Integral a) => t a -> t a
genavg t = xs'
   where
   avg = div s c
   ((s,c),xs') = mapAccumR (\(s',c') b -> ((s' + b , c' + 1) , avg)) (0,0) t

