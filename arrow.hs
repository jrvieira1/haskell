import Prelude hiding ( id, (.) )
import Data.Char ( toUpper )

main :: IO ()
main = runKleisli ka ()
   where
   ka = Kleisli (\_ -> getLine)
      >>> arr (map toUpper)
      >>> Kleisli putStrLn

class Category cat where
   id :: cat a a
   (.) :: cat b c -> cat a b -> cat a c

infixr 1 >>>, <<<

-- | Right-to-left composition
(<<<) :: Category cat => cat b c -> cat a b -> cat a c
(<<<) = (.)

-- | Left-to-right composition
(>>>) :: Category cat => cat a b -> cat b c -> cat a c
(>>>) = flip (.)

class Category a => Arrow a where
   {-# MINIMAL arr, (first | (***)) #-}
   arr :: (b -> c) -> a b c
   first :: a b c -> a (b,d) (c,d)
   first = (*** id)
   second :: a b c -> a (d,b) (d,c)
   second = (id ***)
   (***) :: a b c -> a b' c' -> a (b,b') (c,c')
   f *** g = first f >>> arr swap >>> first g >>> arr swap
      where
      swap ~(x,y) = (y,x)
   (&&&) :: a b c -> a b c' -> a b (c,c')
   f &&& g = arr (\b -> (b,b)) >>> f *** g

instance Category (->) where
   id x = x
   g . f = \x -> g (f x)

instance Arrow (->) where
   arr = id
   (***) f g ~(x,y) = (f x,g y)

newtype Kleisli m a b = Kleisli { runKleisli :: a -> m b }

deriving instance Functor m => Functor (Kleisli m a)

instance Applicative m => Applicative (Kleisli m a) where
  pure = Kleisli . const . pure
  Kleisli f <*> Kleisli g = Kleisli $ \x -> f x <*> g x
  Kleisli f *> Kleisli g = Kleisli $ \x -> f x *> g x
  Kleisli f <* Kleisli g = Kleisli $ \x -> f x <* g x

-- instance Monad m => Monad (Kleisli m a) where
--    Kleisli f >>= k = Kleisli $ \x -> f x >>= \a -> runKleisli (k a) x

instance Monad m => Category (Kleisli m) where
    id = Kleisli pure
    (Kleisli f) . (Kleisli g) = Kleisli (\b -> g b >>= f)

instance Monad m => Arrow (Kleisli m) where
   arr f = Kleisli (pure . f)
   first (Kleisli f) = Kleisli (\ ~(b,d) -> f b >>= \c -> pure (c,d))
   second (Kleisli f) = Kleisli (\ ~(d,b) -> f b >>= \c -> pure (d,c))

