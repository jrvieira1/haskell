{-# LANGUAGE NoImplicitPrelude #-}

data Void  -- 0
-- There are 0 ways to construct Void

data Unit = Unit  -- 1
-- Unit has 1 inhabitant: Unit

data Bool = Fals | True  -- 1+1
-- Bool has 2. We can simplify the algebraic expression 1+1 to 2

data Self a = Self a  -- a

data What a b = This a | That b  -- a+b
-- Has many as a and b combined. The number of inhabitants looks the same as
-- the algebraic form a+b

data Pair a b = Pair a b  -- a*b
-- Has an inhabitant for each combination of as and bs

{- `a -> b` Has b^a. Why?

   Think about `data Trio = Solo | Twin | Trip`
   Why are there 8 combinations of `Trio -> Bool`, but 9 of `Bool -> Trio`?
   It helps to write out each possible function

   https://codewords.recurse.com/issues/three/algebra-and-calculus-of-algebraic-data-types

-}

-- Algebraic manipulation

-- Consider
data Coin a = Head a | Tail a  -- a+a

-- We can manipulate this using the familiar rules of algebra
-- to the equivalent 2*a:

data Coin' a = Coin' Bool a  -- 2*a

{- Exercise for the reader:

   Explain why `Bool -> a` is not equivalent to `What a a`
   but is equivalent to `Pair a a`
   both algebraically and intuitively

-- Algebra

   Bool -> a : a^2
   What a a : a+a
   Pair a a : a*a

   Bool -> Bool : 2^2

   id
   not
   const True
   const False

-- Intuition

   Bool -> Trio : 3^2

   const Solo
   const Twin
   const Trip
   \b -> if b Solo Twin
   \b -> if b Solo Trip
   \b -> if b Twin Solo
   \b -> if b Twin Trip
   \b -> if b Trip Solo
   \b -> if b Trip Twin

-}

-- Taylor series

data List a = Null | Cons a (List a)

-- We can repeatedly expand the definition
-- by expressing it with algebraic notation

-- L = 1 + a * L
--   = 1 + a * (1 + a * L)
--   = 1 + a + a^2 * (1 + a * L)
--   = 1 + a + a^2 + a^3 * (1 + a * L)
--   = 1 + a + a^2 + a^3 + ...

-- The Taylor series for 1/(1-a) is 1+a+a^2+a^3+...
-- L = 1 + a * L
-- L * (1 - a) = 1
-- L = 1 / (1 - a)
-- L = 1 + a + a^2 + a^3 + ...

{-

Let’s pause for a moment to remember that we’re dealing with types.
And the expression 1 / (1 - a) contains both a negative and a
fractional type, neither of which have a meaning yet. Let’s consider
what they could possibly mean. Algebra tells us that (1 - a) +
a = 1. So, this data types combines either a 1 - a or an a, to
get just 1. What? The fractional type is just as unintelligible
– given a 1 / a and an a, we have… 1. In either case it seems
we get out less than we put in! Some research has been done on
deciphering some meaning from this mess, but I can’t use negative
and fractional types without adopting the additional language
semantics they propose. One final note – though we took some morally
objectionable steps, using types we don’t have any justification
for, we came out fine on the other end.

-}

-- For fun, let’s try our luck with binary trees

data Tree a = Leaf a | Branch (Tree a) (Tree a)  -- T = a + T^2

-- T = a + T^2
--   = a + (a + T^2)^2
--   = a + a^2 + 2aT^2 + T^4
--   ...
--   = a + a^2 + 2a^3 + 5a^4 + 14a^5 + ...

-- Taylor series:
-- T = a + T^2
-- T^2 - T + a = 0
-- T = (1 - sqrt(1 - 4a)) / 2
-- T = a + a^2 + 2a^3 + 5a^4 + 14a^5 + ...

{-

(By the way, those coefficients are the Catalan numbers)
This describes a binary tree as being either a single leaf, the
tree with two leaves, one of the two trees with three leaves,
or one of the five trees with four leaves, and so on. Finally
a reward for our diligence! We can answer the question of how
many ways there are to produce a binary tree with five leaves
(14), or 11 (16796)!

As you might expect, an alternate definition of binary trees which
can be empty produces almost exactly the same result.
Try it for yourself!

-}

data BinaryTree a = Leaf | Branch a (BinaryTree a) (BinaryTree a)

