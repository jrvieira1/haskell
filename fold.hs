
main :: IO ()
main = do
   print $ take 6 $ foldr (:) [] [0..]
-- print $ take 6 $ foldl (flip (:)) [] [0..]  -- hangs
-- print $ take 6 $ foldl' (flip (:)) [] [0..]  -- hangs
   print "done"

{- foldr (:) [] [0..]
   0 : foldr (:) [] [1..]
   0 : 1 : foldr (:) [] [2..]
   0 : 1 : 2 : foldr (:) [] [3..]
-}

{- foldl (flip (:)) [] [0..]
   foldl (flip (:)) (flip (:) [] 0) [1..]
   foldl (flip (:)) (flip (:) (flip (:) [] 0) 1) [2..]
   foldl (flip (:)) (flip (:) (flip (:) (flip (:) [] 0) 1) 2) [3..]
-}

{- foldl' (flip (:)) [] [0..]
   foldl' (flip (:)) [0] [1..]
   foldl' (flip (:)) [1,0] [2..]
   foldl' (flip (:)) [2,1,0] [3..]
-}
