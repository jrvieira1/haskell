import Control.Monad ( join )

main :: IO ()
main = do
   print $ join (*) 3
   print $ ((*) >>= id) 3
   print $ (\r -> id ((*) r) r) 3
   print $ (\r -> (*) r r) 3

   print $ ((*) >>= seq) 3
   print $ ((*) >>= (succ .)) 3
   print $ ((*) >>= (. succ)) 3

{-

instance Monad ((->) r) where
   f >>= k = \r -> k (f r) r

join :: Monad m => m (m a) -> m a
join x = x >>= id

-- then

join (*)
=
(*) >>= id
=
\r -> id ((*) r) r
=
\r -> (^) r r

-}
