#! /usr/bin/env cabal
{- cabal:
build-depends: base,
               ansi-terminal-game == 1.9.*
ghc-options: -threaded
-}
import Terminal.Game
main :: IO ()
main = playGame_ game
game :: Game Animation ()
game = Game 13 ani lf df
    where
        ani = creaAnimation $
                map (\c -> (3, box 20 10 c))
                    (concat $ replicate 3 "\\-/|")
        lf _ a k
          | isExpired a && k == (KeyPress 'q') = Left ()
          | otherwise = Right $ tick a
        df e a
          | isExpired a = centerFull e (word "Press 'q' to quit")
          | otherwise = centerFull e (fetchFrame a)
