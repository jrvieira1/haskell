module Verse.Logic where

import Data.IntMap ( IntMap )
import Data.IntMap qualified as IntMap
import Data.Map ( Map )
import Data.Map qualified as Map
import Data.Bifunctor ( bimap )

import System.IO.Unsafe (unsafePerformIO)

data Memo = Memo
   { memo_move :: Map Bool Bool
   , test :: String
   }

intmap = IntMap.fromDistinctAscList . zip [0..]

main :: IO ()
main = do
   print "ok"
   mapM_ print x
   where

   x = sim $ intmap ["zero","um","dois","tres","quatro","cinco","seis","sete","oito","nove","dez","onze","doze"]

-- | Per Node step in simulation

sim :: IntMap String -> IntMap String
sim st = st'
   where

   (m',st') = bimap intmap intmap $ IntMap.foldrWithKey go (mempty,mempty) st
--  ^future Memo

   {- NOTE
   -- m' comes from the future
   -- avoid time travel paradoxes -}
   go :: Int -> String -> ([Memo],[String]) -> ([Memo],[String])
   go i s (macc,vacc) = (m : macc , s' : vacc)
      where

      s' :: String
      s' = unwords [s , test $ m' IntMap.! div i 3]

      -- update memo
      m :: Memo
      m = Memo
         { memo_move = mempty
         , test = unsafePerformIO $ do
            putStr $ "\ESC[34m" <> s <> "\ESC[0m "
            pure $ s
         }

