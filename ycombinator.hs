module Main (main) where

main :: IO ()
main = print $ y (1:)

-- does not typecheck
-- y = \f -> (\x -> f $ x x) (\x -> f $ x x)
-- y f = g g where g x = f $ x x
-- y f = f . y f

newtype Mu a = Mu (Mu a -> a)

-- Omega combinator
w :: (Mu a -> a) -> a
w f = f $ Mu f

y :: (a -> a) -> a
y f = w $ \(Mu x) -> f $ w x
-- y f = (\h -> h $ Mu h) (\x -> f $ (\(Mu g) -> g) x x)

-- using recursion
fix f = x where x = f x

