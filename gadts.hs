{-# LANGUAGE GADTs #-}

main :: IO ()
main = do
   print $ Some "ok"
   print $ at record

data Will a where
   Some :: a -> Will a
   None :: Will a
   deriving Show

data Record a where
   Record :: { nat :: Word , at :: a } -> Record a

record :: Record (Will ())
record = Record 7 None
