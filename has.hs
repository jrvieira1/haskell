main :: IO ()
main = do
   print $ "ok"
   print $ intersperse 6 x
   print $ join x y
   print $ zyp x y
   where
   x = 1 :/ 2 :/ 3 :/ Nil :: List Int
   y = 7 :/ 8 :/ 9 :/ Nil :: List Int

data Will a = None | Some a

infixr :/
data List a = Nil | a :/ List a deriving Show

intersperse :: a -> List a -> List a
intersperse _ Nil = Nil
intersperse el (a :/ as) = a :/ el :/ intersperse el as

join :: List a -> List a -> List a
join Nil y = y
join (a :/ as) y = a :/ join as y

zyp :: List a -> List a -> List a
zyp (a :/ as) (b :/ bs) = a :/ b :/ zyp as bs
zyp Nil _ = Nil
zyp _ Nil = Nil
