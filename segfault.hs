import System.IO.Unsafe ( unsafePerformIO )
import Foreign.Ptr ( Ptr, nullPtr )
import Foreign.Storable ( peek )

main :: IO Word
main = pure $ unsafePerformIO $ peek nullPtr

