-- https://gitlab.haskell.org/ghc/ghc/-/issues/25456
{-# LANGUAGE PatternSynonyms #-}

data R = R { x :: () } deriving Show

pattern P :: R
pattern P = R { x = () }

main :: IO ()
main = do

-- print  P  { x = () }  -- error
   print (P) { x = () }

   print  R  { x = () }
-- print (R) { x = () }  -- error
