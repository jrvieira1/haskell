main :: IO ()
main = do
   print "ok"


-- conjunction
data And a b = And a b  -- (,)
-- disjunction
data Or a b = OrLeft a | OrRight b  -- Either
-- true
data Unit = Unit
-- false
data Bottom

-- q: what is the proposition corresponding to  given type?
-- a: the proposition is "this type is inhabited"

-- INHABITATION

-- for a concrete type, inhabitation is trivial
-- this type is inhabited:
i :: Word
i = 7

-- haskell types are universally quantified
-- the type x :: a is uninhabited

-- for a function a -> b, the proposition is
-- "if a is inhabited, then so is b"

returnList :: a -> [a]
returnList x = [x]

-- PROOFS AS PROGRAMS

-- any value (this normally means a function)
-- inhabiting a given type is a proof of
-- the corresponding proposition

-- constructors = introduction rules in logic
-- pattern matching = elimination rules in logic

-- Modus Ponens

modusPonens :: (a -> b) -> a -> b
modusPonens f x = f x  -- application

-- Hypothetical Syllogism

hypotheticalSyllogism :: (b -> c) -> (a -> b) -> a -> c
hypotheticalSyllogism f g x = f (g x)  -- composition

-- Ex Falso Quodlibet

absurd :: Bottom -> a
absurd x = case x of {}

-- Negation

type Not a = a -> Bottom

-- Contrapositive

contrapositive :: (a -> b) -> Not b -> Not a
contrapositive f notY = notY . f

-- De Morgan I
-- the conjunction of negations implies
-- the negation of the disjuction

deMorganI :: (Not a, Not b) -> Not (Either a b)
deMorganI (notX, _) (Left x) = notX x
deMorganI (_, notY) (Right y) = notY y

-- De Morgan II
-- the disjunction of negations implies
-- the negation of the conjunction

deMorganII :: Either (Not a) (Not b) -> Not (a,b)
deMorganII (Left notX) (x,_) = notX x
deMorganII (Right notY) (_,y) = notY y

-- Eliminating Double Negation
-- Not (Not a) -> a
-- intuitionistic logic doesn't have
-- an elimination rule for double negation
-- (classical true but not intuitionistically)

-- Exluded Middle
-- Either a (Not a)  -- p V ~p
-- intuitionistic logic doesn't have
-- the law of excluded middle
-- (classical true but not intuitionistically)
