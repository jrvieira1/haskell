main = print "ok"

data Free f a where
  Pure :: a -> Free f a
  Free :: f (Free f a) -> Free f a

instance Functor f => Functor (Free f) where
  fmap f (Pure x) = Pure $ f x
  fmap f (Free g) = Free $ fmap f <$> g

instance Functor f => Applicative (Free f) where
  pure = Pure
  Pure f <*> x = fmap f x
  Free g <*> x = Free $ (<*> x) <$> g

instance Functor f => Monad (Free f) where
  Pure x >>= f = f x
  Free g >>= f = Free $ (>>= f) <$> g
